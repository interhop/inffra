# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see https://www.gnu.org/licenses/.

- name: Configure MongoDB repository key
  apt_key:
    url: 'https://www.mongodb.org/static/pgp/server-{{ mongodb_version }}.asc'
    id: 64C3C388
    state: present
  become: yes

- name: Configure MongoDB repository
  apt_repository:
    repo: 'deb http://repo.mongodb.org/apt/debian {{ ansible_distribution_release }}/mongodb-org/{{ mongodb_version }} main'
    state: present
  become: yes

- name: Install packages
  apt:
    update_cache: yes
    pkg:
      - mongodb-org
    state: latest
  become: yes

- name: Start MongoDB service
  service:
    name: mongod
    state: started
    enabled: yes
  become: yes

- name: Create backup directories
  file:
    path: '{{ item.path }}'
    state: directory
    owner: root
    group: root
    mode: '{{ item.mode }}'
  loop:
    - path: '{{ mongodb_backup_path }}'
      mode: '0755'
    - path: '{{ mongodb_backup_path }}/backups'
      mode: '0700'
  become: yes

- name: Create backup script
  template:
    src: backup.sh.j2
    dest: '{{ mongodb_backup_path }}/backup.sh'
    owner: root
    group: root
    mode: '0755'
  become: yes

- name: Install scheduled backup systemd units
  template:
    src: '{{ item }}.j2'
    dest: '/etc/systemd/system/{{ item }}'
    owner: root
    group: root
    mode: '0644'
  loop:
    - mongodb_backup.service
    - mongodb_backup.timer
  become: yes

- name: Reload systemd units
  systemd:
    daemon_reload: yes
  when: 'not ansible_check_mode'
  become: yes

- name: Enabled scheduled backups
  service:
    name: mongodb_backup.timer
    state: started
    enabled: yes
  become: yes
