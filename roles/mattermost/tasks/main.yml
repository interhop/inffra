# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see https://www.gnu.org/licenses/.

- name: Configure mattermost user
  user:
    name: mattermost
    state: present
  become: yes

- name: Prepare Mattermost directories
  file:
    path: '{{ item.path }}'
    state: directory
    owner: '{{ item.owner }}'
    group: '{{ item.group }}'
    mode: '0755'
  loop:
    - path: '{{ mattermost_root_path }}'
      owner: root
      group: root
    - path: '{{ mattermost_root_path }}/{{ mattermost_name }}'
      owner: root
      group: root
    - path: '{{ mattermost_root_path }}/{{ mattermost_name }}/app'
      owner: mattermost
      group: mattermost
    - path: '{{ mattermost_root_path }}/{{ mattermost_name }}/data'
      owner: mattermost
      group: mattermost
  become: yes

- name: Fetch Mattermost archive
  get_url:
    url: '{{ mattermost_url }}'
    dest: '{{ mattermost_root_path }}/{{ mattermost_name }}.tar.gz'
    checksum: '{{ mattermost_checksum }}'
  become: yes

- name: Install Mattermost
  unarchive:
    src: '{{ mattermost_root_path }}/{{ mattermost_name }}.tar.gz'
    dest: '{{ mattermost_root_path }}/{{ mattermost_name }}/app'
    remote_src: yes
    owner: mattermost
    group: mattermost
    mode: 'u=rwX,g=rX,o=rX'
    extra_opts:
      - '--strip-components=1'
      - '--exclude=mattermost/config/config.json'
  become: yes

- name: Install Mattermost service file
  template:
    src: mattermost@.service.j2
    dest: '/etc/systemd/system/mattermost@.service'
    owner: root
    group: root
    mode: '0644'
  become: yes

- name: Configure Mattermost
  template:
    src: config.json.j2
    dest: '{{ mattermost_root_path }}/{{ mattermost_name }}/app/config/config.json'
    owner: mattermost
    group: mattermost
    mode: '0644'
  become: yes

- name: Reload systemd units
  systemd:
    daemon_reload: yes
  when: 'not ansible_check_mode'
  become: yes

- name: Restart Mattermost services
  service:
    name: 'mattermost@{{ mattermost_name }}'
    state: restarted
    enabled: yes
  when: 'not ansible_check_mode'
  become: yes
