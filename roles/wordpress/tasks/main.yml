# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see https://www.gnu.org/licenses/.

- name: Fetch SURY repository key
  get_url:
    url: https://packages.sury.org/php/apt.gpg
    dest: /usr/share/keyrings/deb.sury.org-php.gpg
  become: yes

- name: Configure SURY repository
  apt_repository:
    repo: 'deb [signed-by=/usr/share/keyrings/deb.sury.org-php.gpg] https://packages.sury.org/php/ {{ ansible_distribution_release }} main'
    state: present
  become: yes

- name: Install packages
  apt:
    update_cache: yes
    pkg:
      - php8.2-cli
      - php8.2-mysql
      - php8.2-curl
      - php8.2-gd
      - php8.2-intl
      - php8.2-xml
      - php8.2-sqlite3
      - php8.2-fpm
      - php8.2-imagick
      - php8.2-mbstring
      - php8.2-zip
    state: latest
  become: yes

- name: Prepare WordPress base directory
  file:
    path: '{{ item.path }}'
    state: directory
    owner: '{{ item.owner }}'
    group: '{{ item.group }}'
    mode: '0755'
  loop:
    - path: '{{ wordpress_root_path }}'
      owner: root
      group: root
    - path: '{{ wordpress_root_path }}/{{ wordpress_name }}'
      owner: root
      group: root
    - path: '{{ wordpress_root_path }}/{{ wordpress_name }}/new'
      owner: root
      group: root
    - path: '{{ wordpress_root_path }}/{{ wordpress_name }}/wordpress'
      owner: www-data
      group: www-data
  become: yes

- name: Fetch WordPress archive
  get_url:
    url: '{{ wordpress_url }}'
    dest: '{{ wordpress_root_path }}/{{ wordpress_name }}.tar.gz'
    checksum: '{{ wordpress_checksum }}'
  become: yes

- name: Extract WordPress archive
  unarchive:
    src: '{{ wordpress_root_path }}/{{ wordpress_name }}.tar.gz'
    dest: '{{ wordpress_root_path }}/{{ wordpress_name }}/new'
    remote_src: yes
    owner: www-data
    group: www-data
    mode: 'u=rwX,g=rX,o=rX'
    extra_opts:
      - '--strip-components=1'
  become: yes

- name: Synchronize WordPress install
  synchronize:
    src: '{{ wordpress_root_path }}/{{ wordpress_name }}/new/'
    dest: '{{ wordpress_root_path }}/{{ wordpress_name }}/wordpress/'
    delete: yes
    recursive: yes
    owner: true
    group: true
    rsync_opts:
      - "--exclude=wp-content"
  delegate_to: '{{ inventory_hostname }}'
  become: yes

- name: Generate random salts
  block:
  - name: Generate random salts 1/4
    stat:
      path: '{{ wordpress_root_path }}/{{ wordpress_name }}/salt'
    register: salt1
    become: yes
  - name: Generate random salts 2/4
    get_url:
      url: 'https://api.wordpress.org/secret-key/1.1/salt'
      dest: '{{ wordpress_root_path }}/{{ wordpress_name }}/salt'
      owner: root
      group: root
      mode: '0644'
    when: 'not salt1.stat.exists'
    become: yes
  - name: Generate random salts 3/4
    slurp:
      src: '{{ wordpress_root_path }}/{{ wordpress_name }}/salt'
    register: salt2
    become: yes
  - name: Generate random salts 4/4
    set_fact:
      wordpress_salts: '{{ salt2.content | b64decode }}'

- name: Configure WordPress
  template:
    src: wp-config.php.j2
    dest: '{{ wordpress_root_path }}/{{ wordpress_name }}/wordpress/wp-config.php'
    owner: root
    group: root
    mode: '0644'
  become: yes

- name: Configure PHP-FPM pool
  template:
    src: fpm.conf.j2
    dest: /etc/php/8.2/fpm/pool.d/wordpress.conf
    owner: root
    group: root
    mode: '0644'
  become: yes

- name: Restart PHP-FPM service
  service:
    name: php8.2-fpm
    state: restarted
    enabled: yes
  when: 'not ansible_check_mode'
  become: yes
